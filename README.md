## Description  
Precor Connect service responsible for spiff reviews.

## Features

##### Create List of Spiff Logs
* [documentation](src/root/api/features/CreateSpiffLogs.feature)

##### List Spiff Logs
* [documentation](src/root/api/features/GetSpiffLogs.feature)

##### Update Spiff Logs
* [documentation](src/root/api/features/UpdateSpiffLogs.feature)


## APIs
* [REST API](src/root/web-api/README.md)  

## SDKs  
* [SDK for Javascript](https://bitbucket.org/precorconnect/spiff-review-service-sdk-for-javascript)

## Configuration
Configuration is obtained through the environment variables listed below.
note: environment variables prefixed with `TEST_` are used only in integration tests.

|name|
|---|
|DATABASE_URI|
|DATABASE_USERNAME|
|DATABASE_PASSWORD|
|PRECOR_CONNECT_API_BASE_URL|
|TEST_IDENTITY_SERVICE_JWT_SIGNING_KEY|

## Develop:

#### Software
- git
- java 8 JDK
- maven
- docker toolbox

#### Scripts

set environment variables (perform prior to running or integration testing locally)
```PowerShell
 .\src\root\set-environment.ps1
```

compile & unit test
```PowerShell
mvn test -f .\src\root\pom.xml
```

compile & integration test
```PowerShell
mvn integration-test -f .\src\root\pom.xml
```

create docker image
```PowerShell
 mvn install -f .\src\root\pom.xml
```

run
```PowerShell
java -jar .\src\root\web-api\target\web-api.jar
```

run in docker container
```PowerShell
docker run -P claim-spiff-service
```

